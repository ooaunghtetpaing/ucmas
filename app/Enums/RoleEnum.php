<?php

namespace App\Enums;

enum RoleEnum: string
{
    case SUPER_ADMIN = 'SUPER_ADMIN';
    case OWNER = 'OWNER';
    case TEACHER = 'TEACHER';
    case STUDENT = 'STUDENT';
    case ADMIN = 'ADMIN';
}
