<?php

use App\Enums\PermissionEnum;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BusinessController;
use App\Http\Controllers\ContactPersonController;
use App\Http\Controllers\CustomerAddressController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\GeneralItemController;
use App\Http\Controllers\ItemCategoryController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', [AuthController::class, 'login']);
});

Route::middleware('jwt')->group(function () {

    Route::group(['prefix' => 'user'], function () {
        Route::post('/', [UserController::class, 'store'])->permission(PermissionEnum::USER_STORE->value);
        Route::get('/', [UserController::class, 'index'])->permission(PermissionEnum::USER_INDEX->value);
        Route::get('/{id}', [UserController::class, 'show'])->permission(PermissionEnum::USER_SHOW->value);
        Route::put('/{id}', [UserController::class, 'update'])->permission(PermissionEnum::USER_UPDATE->value);
        Route::delete('/{id}', [UserController::class, 'destroy'])->permission(PermissionEnum::USER_DESTROY->value);
    });

    Route::group(['prefix' => 'item'], function () {

        Route::group(['prefix' => 'general'], function () {
            Route::post('/', [GeneralItemController::class, 'store'])->permission(PermissionEnum::GENERAL_ITEM_STORE->value);
            Route::get('/', [GeneralItemController::class, 'index'])->permission(PermissionEnum::GENERAL_ITEM_STORE->value);
            Route::get('/{id}', [GeneralItemController::class, 'show'])->permission(PermissionEnum::GENERAL_ITEM_STORE->value);
            Route::put('/{id}', [GeneralItemController::class, 'update'])->permission(PermissionEnum::GENERAL_ITEM_STORE->value);
            Route::delete('/{id}', [GeneralItemController::class, 'destroy'])->permission(PermissionEnum::GENERAL_ITEM_STORE->value);
        });

        Route::group(['prefix' => 'category'], function () {
            Route::post('/', [ItemCategoryController::class, 'store'])->permission(PermissionEnum::ITEM_CATEGORY_STORE->value);
            Route::get('/', [ItemCategoryController::class, 'index'])->permission(PermissionEnum::ITEM_CATEGORY_INDEX->value);
            Route::get('/{id}', [ItemCategoryController::class, 'show'])->permission(PermissionEnum::ITEM_CATEGORY_SHOW->value);
            Route::put('/{id}', [ItemCategoryController::class, 'update'])->permission(PermissionEnum::ITEM_CATEGORY_UPDATE->value);
            Route::delete('/{id}', [ItemCategoryController::class, 'destroy'])->permission(PermissionEnum::ITEM_CATEGORY_DESTROY->value);
        });
    });

    Route::group(['prefix' => 'tag'], function () {
        Route::post('/', [TagController::class, 'store'])->permission(PermissionEnum::TAG_STORE->value);
        Route::get('/', [TagController::class, 'index'])->permission(PermissionEnum::TAG_INDEX->value);
        Route::get('/{id}', [TagController::class, 'show'])->permission(PermissionEnum::TAG_SHOW->value);
        Route::put('/{id}', [TagController::class, 'update'])->permission(PermissionEnum::TAG_UPDATE->value);
        Route::delete('/{id}', [TagController::class, 'destroy'])->permission(PermissionEnum::TAG_DESTROY->value);
    });

    Route::group(['prefix' => 'customer'], function () {

        Route::group(['prefix' => 'address'], function () {
            Route::post('/', [CustomerAddressController::class, 'store'])->permission(PermissionEnum::CUSTOMER_ADDRESS_STORE->value);
            Route::get('/', [CustomerAddressController::class, 'index'])->permission(PermissionEnum::CUSTOMER_ADDRESS_INDEX->value);
            Route::get('/{id}', [CustomerAddressController::class, 'show'])->permission(PermissionEnum::CUSTOMER_ADDRESS_SHOW->value);
            Route::put('/{id}', [CustomerAddressController::class, 'update'])->permission(PermissionEnum::CUSTOMER_ADDRESS_UPDATE->value);
            Route::delete('/{id}', [CustomerAddressController::class, 'destroy'])->permission(PermissionEnum::CUSTOMER_ADDRESS_DESTROY->value);
        });

        Route::post('/', [CustomerController::class, 'store'])->permission(PermissionEnum::CUSTOMER_STORE->value);
        Route::get('/', [CustomerController::class, 'index'])->permission(PermissionEnum::CUSTOMER_INDEX->value);
        Route::get('/{id}', [CustomerController::class, 'show'])->permission(PermissionEnum::CUSTOMER_SHOW->value);
        Route::put('/{id}', [CustomerController::class, 'update'])->permission(PermissionEnum::CUSTOMER_UPDATE->value);
        Route::delete('/{id}', [CustomerController::class, 'destroy'])->permission(PermissionEnum::CUSTOMER_DESTROY->value);

    });

    Route::group(['prefix' => 'supplier'], function () {

        Route::group(['prefix' => 'business'], function () {
            Route::post('/', [BusinessController::class, 'store'])->permission(PermissionEnum::BUSINESS_STORE->value);
            Route::get('/', [BusinessController::class, 'index'])->permission(PermissionEnum::BUSINESS_INDEX->value);
            Route::get('/{id}', [BusinessController::class, 'show'])->permission(PermissionEnum::BUSINESS_SHOW->value);
            Route::put('/{id}', [BusinessController::class, 'update'])->permission(PermissionEnum::BUSINESS_UPDATE->value);
            Route::delete('/{id}', [BusinessController::class, 'destroy'])->permission(PermissionEnum::BUSINESS_DESTROY->value);
        });

        Route::group(['prefix' => 'contact-person'], function () {
            Route::post('/', [ContactPersonController::class, 'store'])->permission(PermissionEnum::CONTACT_PERSON_STORE->value);
            Route::get('/', [ContactPersonController::class, 'index'])->permission(PermissionEnum::CONTACT_PERSON_INDEX->value);
            Route::get('/{id}', [ContactPersonController::class, 'show'])->permission(PermissionEnum::CONTACT_PERSON_SHOW->value);
            Route::put('/{id}', [ContactPersonController::class, 'update'])->permission(PermissionEnum::CONTACT_PERSON_UPDATE->value);
            Route::delete('/{id}', [ContactPersonController::class, 'destroy'])->permission(PermissionEnum::CONTACT_PERSON_DESTROY->value);
        });
    });

    Route::group(['prefix' => 'employee'], function () {
        Route::post('/', [EmployeeController::class, 'store'])->permission(PermissionEnum::EMPLOYEE_STORE->value);
        Route::get('/', [EmployeeController::class, 'index'])->permission(PermissionEnum::EMPLOYEE_INDEX->value);
        Route::get('/{id}', [EmployeeController::class, 'show'])->permission(PermissionEnum::EMPLOYEE_SHOW->value);
        Route::put('/{id}', [EmployeeController::class, 'update'])->permission(PermissionEnum::EMPLOYEE_UPDATE->value);
        Route::delete('/{id}', [EmployeeController::class, 'destroy'])->permission(PermissionEnum::EMPLOYEE_DESTROY->value);
    });
});
